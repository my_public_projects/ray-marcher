#include <iostream>
#include <SFML/Graphics.hpp>
#include <math.h>
#include <assert.h>
#include "thread_pool.h"

//--------------INIT--------------
const size_t width = 640;
const size_t height = 480;
//static float fov = M_PI * 120.f / 180.f;
const float view_dist = 1000.f;
static float min_dist = 0.0013;
static sf::Vector3f cam = {0,0, 35};
static float cam_angle = 0;

static multithreading::thread_pool pool;

static std::mutex mutex;
static std::condition_variable cond;
static size_t done_jobs;

static std::vector<std::vector<sf::Vector3f>> screen;

void fov_recalc(std::vector<std::vector<sf::Vector3f>> & screen, float fov)
{
    screen.resize(height);
    for(size_t y = 0; y < height; ++y)
    {
        screen[y].resize(width);
        for(size_t x = 0; x < width; ++x)
        {
            auto & dir = screen[y][x];
            dir.x =  (2*(x + 0.5)/(float)width  - 1)*tan(fov/2.)*width/(float)height;
            dir.y = -(2*(y + 0.5)/(float)height - 1)*tan(fov/2.);
            dir.z = -1;
        }
    }
};

template< typename T>
T make()
{
    T res;
    res.create(width, height);
    return res;
}

static sf::Image image =  make<sf::Image>() ;
static sf::Texture texture = make<sf::Texture>();

static sf::RenderWindow window(sf::VideoMode(width, height), "Rays");

//--------------VEC3 math--------------

inline float s_len(const sf::Vector3f& a)
{
    return a.x*a.x + a.y*a.y + a.z*a.z;
}

inline float s_distance(const sf::Vector3f & a, const sf::Vector3f & b)
{
    auto d = a - b;
    return s_len(d);
}

inline float distance(const sf::Vector3f & a, const sf::Vector3f & b)
{
    return std::sqrt(s_distance(a,b));
}

inline float len(const sf::Vector3f & a)
{
    return std::sqrt(s_len(a));
}

inline sf::Color color_fade(const sf::Color & src, float count)
{
    sf::Color c = src;
    c.r*=count;
    c.g*=count;
    c.b*=count;
    return c;
}

inline float dot(const sf::Vector3f & a, const sf::Vector3f & b)
{
    return a.x*b.x + a.y*b.y + a.z*b.z;
}

inline sf::Vector3f normalize(const sf::Vector3f & dir)
{
    float invers_length = 1.f / std::sqrt(
                                    dir.x * dir.x +
                                    dir.y * dir.y +
                                    dir.z * dir.z);
    return  dir * invers_length;
}

inline sf::Vector3f  max(const sf::Vector3f & v, float d)
{
   sf::Vector3f res;
   res.x = std::max(v.x, d);
   res.y = std::max(v.y, d);
   res.z = std::max(v.z, d);
   return res;
}

inline sf::Vector3f rotateXZ(const sf::Vector3f & vec, float rad_angle)
{
    return
    {vec.x * std::cos(rad_angle) - vec.z * std::sin(rad_angle),
     vec.y,
    vec.x * std::sin(rad_angle) +  vec.z * std::cos(rad_angle)};
}

//--------------Shapes--------------
struct objct
{
    sf::Vector3f pos;
    sf::Color color;
    virtual float ray_intersect(const sf::Vector3f & dir) const = 0;
    virtual ~objct(){}
};


struct sphere : public objct
{
    float radius =  20;

    sphere()
    {
        color = sf::Color(255, 0,0);
    }

    ~sphere() override {}

    float ray_intersect(const sf::Vector3f &dir) const override
    {
        return  distance(pos, dir) - radius;
    }
};

struct plane : public objct
{

    plane()
    {
        color = sf::Color(0, 120,120);
    }

    ~plane() override {}

    float ray_intersect(const sf::Vector3f & dir) const override
    {
        return  dir.y - pos.y;
    }
};

struct box : public objct
{

    sf::Vector3f size = {10, 15, 12};

    box()
    {
        pos = {30, 10, -50};
        color = sf::Color(0, 255,0);
    }

    ~box() override{}

    float ray_intersect(const sf::Vector3f & dir) const override
    {

        auto temp = dir - pos ;
        sf::Vector3f q =
                sf::Vector3f(
                    std::abs(temp.x),
                    std::abs(temp.y),
                    std::abs(temp.z)) - size;

        return len(max(q, 0.f)) + std::min(std::max(q.x,std::max(q.y,q.z)), 0.f);
    }
};

//--------------RAY MAARCHING--------------
std::pair<bool, sf::Vector3f>
ray_cast(
        const sf::Vector3f & dir,
        const objct* object
        )
{
    auto temp = cam;
    float old_dist = std::numeric_limits<float>().max();
    float dist = 0;

    for(size_t i = 0; i < 500; ++i)
    {
        dist = object->ray_intersect(temp);;
        if(dist > view_dist)
        {
            return std::pair(false, sf::Vector3f());
        }

        // bad optimization/
        if(dist > old_dist)
        {
            return std::pair(false, sf::Vector3f());
        }

        temp += dir * dist;
        if(dist < min_dist)
        {
            return std::pair(true, temp);
        }



        old_dist = dist;

    }

    return std::pair(false, sf::Vector3f());
}

sf::Color get_light(
        const sf::Vector3f & point,
        const sf::Color& color, const std::vector<sf::Vector3f> & lights,
        const objct* obj)
{

    int r = 0;
    int g = 0;
    int b = 0;

    for(const auto & l: lights)
    {
       sf::Vector3f s_n;
       const float eps = 0.001f;
       auto temp = point ;
        s_n.x = obj->ray_intersect(sf::Vector3f(temp.x + eps, temp.y, temp.z)) -
                obj->ray_intersect(temp);

        s_n.y = obj->ray_intersect(sf::Vector3f(temp.x, temp.y + eps, temp.z)) -
                obj->ray_intersect(temp);
        s_n.z = obj->ray_intersect(sf::Vector3f(temp.x, temp.y, temp.z + eps)) -
                obj->ray_intersect(temp);
        s_n = normalize(s_n);
        temp = normalize(l - point );

        float intense = dot(s_n, temp);

        if(intense < 0) continue;

        r += color.r * intense;
        g += color.g * intense;
        b += color.b * intense;

    }

    r = r > 255 ? 255 : r < 0 ? 0 : r;
    g = g > 255 ? 255 : g < 0 ? 0 : g;
    b = b > 255 ? 255 : b < 0 ? 0 : b;



    return sf::Color(r,g,b);

}

void render(
        std::vector<objct*> & objs,
        const std::vector<sf::Vector3f> & lights, size_t from, size_t n)
{
    std::vector<std::pair<sf::Vector3f, const objct*>> results;
    for (size_t j = from; j < from + n; j++) { // actual rendering loop
        for (size_t i = 0; i < width; i++) {
            sf::Vector3f dir;

            dir = rotateXZ(screen[j][i], cam_angle);

            dir = normalize(dir);

            results.clear();
            for(const auto * obj : objs)
            {
                auto res = ray_cast(dir, obj);
                if(res.first)
                {
                   results.push_back(std::pair(res.second, obj));
                }
            }

            if(results.size())
            {
                std::sort(results.begin(), results.end(),
                          [](const std::pair<sf::Vector3f, const objct*> & a,
                          const std::pair<sf::Vector3f, const objct*> b)
                {
                        return s_distance(a.first, cam) <
                                s_distance(b.first, cam);
                });

                const auto & res = results.front();

                auto lighted_color = get_light(
                            res.first,
                            res.second->color,
                            lights,
                            res.second);


                image.setPixel(i, j, lighted_color);

            }
            else
            {
                image.setPixel(i, j, color_fade(sf::Color(0,120,200), 1.f - float(j) / height));
            }

        }
    }

    std::lock_guard<std::mutex> lk(mutex);
    ++done_jobs;
    cond.notify_one();

}

//--------------FILL and CLEAR--------------
void fill(std::vector<objct*> & objs, int w = 10, int h = 5)
{
    for(int i = 0; i < h; ++i)
    {
        for(int j = 0; j < w; ++j)
        {
            box * s = new box();
            s->size = {10.f, 10.f, 10.f};
            s->pos = sf::Vector3f(j * 25 - (w*25)/2, i * 25, -250);
            s->color = sf::Color(rand() % 0xFFFFFFFF);
            s->color.a = 255;
            objs.push_back(s);
        }
    }
}

void clean(std::vector<objct*> & objs)
{
    for(auto o: objs)
    {
        delete o;
    }
    objs.clear();
}

//--------------
int main()
{
    std::cout << "Hello World!\n";

    float fov = M_PI * 120.f /180;

    fov_recalc(screen, fov);
    bool fov_rec = false;

    std::vector<objct*> objs;
    std::vector<sf::Vector3f> lights;
    lights.resize(3);


    sphere * sp1   = new sphere();
    sp1->pos = {10,0,-100};
    objs.push_back(sp1);

    sphere * sp2   = new sphere();
    sp2->pos = {-10,0,-100};
    sp2->color = {0,0,255};
    objs.push_back(sp2);


    sphere * sp3   = new sphere();
    sp3->pos = {-10,0,-100};
    sp3->color = {0,120,255};
    objs.push_back(sp3);


    plane * pl = new plane();

    pl->pos = {0, -10, 0};
    objs.push_back(pl);

    box * b1 = new box();
    objs.push_back(b1);

    fill(objs, 5,5);

    sf::Event e;
    sf::Sprite pict;
    pict.setTexture(texture);
    float time = 0;
    const size_t count = pool.get_threads_count();
    const size_t per_job = height / count;
    const size_t n_jobs = height / per_job - 1;


    window.setFramerateLimit(25);



    while(1)
    {
        window.pollEvent(e);
        if(e.type == sf::Event::Closed)
        {
            return 0;
        }

        window.clear();

        done_jobs = 0;
        size_t from =0;
        for(size_t i = 0; i < n_jobs; ++i)
        {
            pool.submit(std::bind(&render, objs, lights, from, per_job));
            from += per_job;
        }


        pool.submit(std::bind(&render, objs, lights, from, height - from));

        { //Comment this block for fun
            std::unique_lock<std::mutex> lk(mutex);
            cond.wait(lk, [n_jobs](){return done_jobs == n_jobs+1;});
        }

        texture.update(image);
        window.draw(pict);
        window.display();

        const sf::Vector3f speed = {0,0,-5};

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
        {
            //min_dist += 0.1f;
            cam += rotateXZ(speed, cam_angle);
        }

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
        {
            //min_dist -= 0.1f;
            cam -= rotateXZ(speed, cam_angle);
        }

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
        {
            //min_dist += 0.1f;
            cam_angle -= M_PI * 5.f / 180.f;
            cam_angle = cam_angle < 0 ? 2*M_PI : cam_angle;
        }

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
        {
            //min_dist -= 0.1f;
            cam_angle += M_PI * 5.f / 180.f;
            cam_angle  = cam_angle > 2*M_PI ? 0 : cam_angle;

        }

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::LShift))
        {
            //min_dist -= 0.1f;
            fov = M_PI * 70.f /180.f;
            fov_rec = true;
        } else {
            fov = M_PI * 120.f / 180.f;
            fov_rec = true;
        }

        if(fov_rec)
        {
            fov_recalc(screen, fov);
            fov_rec = false;
        }

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
        {
            break;
        }

        //std::cout << "min_dist:" << min_dist << "\n";
        sp1->pos = sf::Vector3f(0,
                                50 * std::sin(8*M_PI+time), -100);


        sp2->pos = sf::Vector3f(50 * std::sin(5*M_PI+time),
                                0,
                                50 * std::cos(5*M_PI+time) -100);


        sp3->pos = sf::Vector3f(50 * std::sin(2*M_PI+time),
                                50 * std::cos(2*M_PI+time),
                                50 * std::sin(2*M_PI+time) -100);

        b1->pos = sf::Vector3f(50 * std::sin(32*M_PI+time),
                                50 * std::cos(2*M_PI+time),
                                50 * std::cos(2*M_PI+time) -100);

        lights[0] = sf::Vector3f(0,
                                 50 * std::sin(8*M_PI+time), -100);

        lights[1] = sf::Vector3f(50 * std::sin(5*M_PI+time),
                                 0,
                                 50 * std::cos(5*M_PI+time) -100);

        lights[2] = sf::Vector3f(50 * std::sin(2*M_PI+time),
                                 50 * std::cos(2*M_PI+time),
                                 50 * std::sin(2*M_PI+time) -100);

//        cam = sf::Vector3f(100 * std::sin(10*M_PI+time),
//                           0,
//                           100 * std::cos(10*M_PI+time) - 100);

        time += 0.01;

    }

    clean(objs);
    return 0;
}
