TEMPLATE = app
unix:QMAKE_CXXFLAGS_GNUCXX11 = -std=c++17
unix:QMAKE_CXXFLAGS_GNUCXX1Z = -std=c++17
win32: QMAKE_CXXFLAGS += /std:c++17
unix: QMAKE_CXXFLAGS += -std=c++17
unix: QMAKE_CXX = g++-8

CONFIG -= app_bundle
CONFIG -= qt
QT -= core
QT -= gui
LIBS += -lsfml-graphics
LIBS += -lsfml-window
LIBS += -lsfml-system

unix:{
    LIBS += -lpthread
}

INCLUDEPATH += $$PWD/../include

HEADERS += \
    ../include/queue.h \
    ../include/thread_pool.h

SOURCES += \
        ../src/main.cpp \
        ../src/thread_pool.cpp


